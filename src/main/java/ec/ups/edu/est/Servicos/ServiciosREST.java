package ec.ups.edu.est.Servicos;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import ec.ups.edu.est.Modelo.Carrito;
import ec.ups.edu.est.Modelo.FacturaCabecera;
import ec.ups.edu.est.Modelo.FacturaDetalle;
import ec.ups.edu.est.Modelo.Producto;
import ec.ups.edu.est.Modelo.Respuesta;
import ec.ups.edu.est.ObjetoDeNegocio.EmpresaONLocal;

@Path("/servicios")
public class ServiciosREST {
	@Inject 
	private EmpresaONLocal onLocal; 
	
	@POST  
	@Path("/ingresarproducto") 
	@Produces("application/json") 
	@Consumes("application/json")  
	public Respuesta guardarProductos(Producto pro) { 
		return onLocal.guardarProducto(pro);
	}
	
	@GET 
	@Path("/obtenerproductos")
	@Produces("application/json")  
	public Respuesta obtenerProductos() { 
		return onLocal.obtenerProductos();
	} 
	
	
	@GET 
	@Path("/obtenerproducto")  
	@Produces("application/json")
	public Producto obtenerProducto(@QueryParam("codigo") int codigoProducto) { 
		return onLocal.buscarProducto(codigoProducto);
	}  
	
	@POST
	@Path("/obtenerFactura")  
	@Produces("application/json")  
	@Consumes("application/json") 
	public FacturaCabecera obtenerFacturaCabecera(List<FacturaDetalle> facturasDetalles) { 
		return onLocal.obtenerFactura(facturasDetalles);
	}
	
	
	@POST
	@Path("/anadirCarrito") 
	@Produces("application/json")  
	@Consumes("application/json")
	public List<FacturaDetalle> carrito(Carrito carrito) {
		return onLocal.añadirCarrito(carrito.getListDetalles(), carrito.getCodigoProducto(), carrito.getCantidadProducto());
	}
	
	
	
}
