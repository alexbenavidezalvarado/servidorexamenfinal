package ec.ups.edu.est.ObjetoDeNegocio;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.ups.edu.ObjetosDeNegocio.FacturaCabeceraDAO;
import ec.ups.edu.ObjetosDeNegocio.ProductoDAO;
import ec.ups.edu.est.Modelo.FacturaCabecera;
import ec.ups.edu.est.Modelo.FacturaDetalle;
import ec.ups.edu.est.Modelo.Producto;
import ec.ups.edu.est.Modelo.Respuesta;

@Stateless
public class EmpresaON implements EmpresaONLocal{ 
	
	@Inject 
	private FacturaCabeceraDAO cabeceraDAO; 
	@Inject 
	private ProductoDAO productoDAO;
	
	public void insertarFacturaCabecera(FacturaCabecera cabecera) { 
		cabeceraDAO.insert(cabecera);
	}  
	
	public void actualizarFacturaCabecera(FacturaCabecera cabecera) { 
		cabeceraDAO.update(cabecera);
	}
	
	public FacturaCabecera buscarFacturaCabecera(int codigoFacturaCabecera) { 
		FacturaCabecera cabecera = cabeceraDAO.read(codigoFacturaCabecera); 
		return cabecera;
	}  
	
	public void eliminarFacturaCabecera (int codigoFacturaCabecera) {  
		cabeceraDAO.delete(codigoFacturaCabecera);
	}
	
	public List<FacturaCabecera> listarFacturasCabeceras(){ 
		List<FacturaCabecera> cabeceras = cabeceraDAO.getFacturaCabeceras(); 
		return cabeceras;
	}     
	
	public void insertarProducto(Producto producto) { 
		productoDAO.insert(producto);
	}  
	
	public void actualizarProducto(Producto producto) { 
		productoDAO.update(producto);
	}
	
	public Producto buscarProducto(int codigoProducto) { 
		Producto p = productoDAO.read(codigoProducto); 
		return p;
	}  
	
	public void eliminarProducto(int codigoProducto) {  
		productoDAO.delete(codigoProducto);
	}
	
	public List<Producto> listarProductos(){ 
		List<Producto> productos = productoDAO.getProductos(); 
		return productos;
	}    
	
	public Respuesta guardarProducto(Producto pro) {  
		Respuesta respuesta = new Respuesta(); 
		try {
			productoDAO.insert(pro);  
			respuesta.setCodigoRespuesta(1); 
			respuesta.setDescripcion("Producto guardado exitosamente");
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(2); 
			respuesta.setDescripcion("Erro " + e.getMessage());
		} 
		
		return respuesta;
	} 
	
	
	public Respuesta obtenerProductos() {  
		Respuesta respuesta = new Respuesta();  
		List<Producto> productos = new ArrayList<Producto>();
		try {
			productos = productoDAO.getProductos();  
			respuesta.setCodigoRespuesta(1); 
			respuesta.setDescripcion("Se han obtenido los productos"); 
			respuesta.setProductos(productos);
		} catch (Exception e) {
			respuesta.setCodigoRespuesta(2); 
			respuesta.setDescripcion("Erro " + e.getMessage());
		} 
		
		return respuesta;
	} 
	
	
	public FacturaCabecera obtenerFactura(List<FacturaDetalle> listaDeatalles) { 
		double total = 0; 
		FacturaCabecera facturaCabecera = new FacturaCabecera(); 
		facturaCabecera.setLstDetalles(listaDeatalles); 
		for(FacturaDetalle facturaDetalle : listaDeatalles) { 
			total = total + facturaDetalle.getSubtotal();
		} 
		facturaCabecera.setTotalFactura(total);
		return facturaCabecera;
	} 
	
	
	public List<FacturaDetalle> añadirCarrito(List<FacturaDetalle> detalles,int codigoProducto, int cantidad){ 
		FacturaDetalle facturaDetalle = new FacturaDetalle(); 
		facturaDetalle.setCantidad(cantidad);
		Producto p = productoDAO.read(codigoProducto); 
		System.out.println(p);
		facturaDetalle.setProducto(p);
		facturaDetalle.setSubtotal(cantidad*p.getPrecio());  
		if(detalles.size()>0) { 
			for(FacturaDetalle detalle : detalles) {  
				if(detalle.getProducto().getCodigoProducto() == p.getCodigoProducto()) { 
					detalle.setCantidad(detalle.getCantidad()+cantidad); 
					detalle.setSubtotal(detalle.getSubtotal()+(detalle.getProducto().getPrecio()*cantidad));
					
				}else {  
					detalles.add(facturaDetalle);
					break;
				}
			} 
		}else { 
			detalles.add(facturaDetalle);
		}
		return detalles;
	}
	
}
