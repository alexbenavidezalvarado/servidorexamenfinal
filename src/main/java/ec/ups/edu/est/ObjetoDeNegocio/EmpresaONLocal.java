package ec.ups.edu.est.ObjetoDeNegocio;

import java.util.List;

import javax.ejb.Local;

import ec.ups.edu.est.Modelo.FacturaCabecera;
import ec.ups.edu.est.Modelo.FacturaDetalle;
import ec.ups.edu.est.Modelo.Producto;
import ec.ups.edu.est.Modelo.Respuesta;

@Local
public interface EmpresaONLocal {
	public void insertarFacturaCabecera(FacturaCabecera cabecera); 
	public void actualizarFacturaCabecera(FacturaCabecera cabecera);  
	public FacturaCabecera buscarFacturaCabecera(int codigoFacturaCabecera); 
	public void eliminarFacturaCabecera (int codigoFacturaCabecera); 
	public List<FacturaCabecera> listarFacturasCabeceras();  
	public void insertarProducto(Producto producto); 
	public void actualizarProducto(Producto producto); 
	public Producto buscarProducto(int codigoProducto); 
	public void eliminarProducto(int codigoProducto); 
	public List<Producto> listarProductos(); 
	public Respuesta guardarProducto(Producto pro); 
	public Respuesta obtenerProductos(); 
	public FacturaCabecera obtenerFactura(List<FacturaDetalle> listaDeatalles); 
	public List<FacturaDetalle> añadirCarrito(List<FacturaDetalle> detalles,int codigoProducto, int cantidad);
	
}
