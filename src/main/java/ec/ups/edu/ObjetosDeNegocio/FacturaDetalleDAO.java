package ec.ups.edu.ObjetosDeNegocio;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.ups.edu.est.Modelo.FacturaCabecera;
import ec.ups.edu.est.Modelo.FacturaDetalle;

public class FacturaDetalleDAO {
	@PersistenceContext 
	private EntityManager em; 
	
	public void insert (FacturaDetalle f) { 
		em.persist(f);
	}
	public void update(FacturaDetalle f) { 
		em.merge(f);
	} 
	
	public FacturaDetalle read(int codigoFacturaDetalle) { 
		return em.find(FacturaDetalle.class, codigoFacturaDetalle);
	} 
	
	public void delete(int codigoFacturaDetalle) { 
		FacturaDetalle f = read(codigoFacturaDetalle); 
		em.remove(f);
	}  
	
	public List<FacturaCabecera> getFacturaCabeceras(){ 
		String jpql = "SELECT f FROM FacturaDetalle f ";
		
		Query q = em.createQuery(jpql,FacturaDetalle.class); 
		return q.getResultList();
	}
}
