package ec.ups.edu.ObjetosDeNegocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.ups.edu.est.Modelo.Producto;



@Stateless
public class ProductoDAO {
	@PersistenceContext 
	private EntityManager em; 
	
	public void insert (Producto p) { 
		em.persist(p);
	}
	public void update(Producto p) { 
		em.merge(p);
	} 
	
	public Producto read(int codigoProducto) { 
		return em.find(Producto.class, codigoProducto);
	} 
	
	public void delete(int codigoProducto) { 
		Producto p = read(codigoProducto); 
		em.remove(p);
	}  
	
	public List<Producto> getProductos(){ 
		String jpql = "SELECT p FROM Producto p ";
		
		Query q = em.createQuery(jpql,Producto.class); 
		return q.getResultList();
	}
}
