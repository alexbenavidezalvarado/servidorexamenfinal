package ec.ups.edu.ObjetosDeNegocio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.ups.edu.est.Modelo.FacturaCabecera;
import ec.ups.edu.est.Modelo.Producto;

@Stateless
public class FacturaCabeceraDAO {
	@PersistenceContext 
	private EntityManager em; 
	
	public void insert (FacturaCabecera f) { 
		em.persist(f);
	}
	public void update(FacturaCabecera f) { 
		em.merge(f);
	} 
	
	public FacturaCabecera read(int codigoFacturaCabecera) { 
		return em.find(FacturaCabecera.class, codigoFacturaCabecera);
	} 
	
	public void delete(int codigoFacturaCabecera) { 
		FacturaCabecera f = read(codigoFacturaCabecera); 
		em.remove(f);
	}  
	
	public List<FacturaCabecera> getFacturaCabeceras(){ 
		String jpql = "SELECT f FROM FacturaCabecera f ";
		
		Query q = em.createQuery(jpql,FacturaCabecera.class); 
		return q.getResultList();
	}
}
